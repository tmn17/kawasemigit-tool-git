import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class main {

    public static final String KEY_PATH_WORKSPACE = "pathWorkspace";
    public static final String KEY_REMOTE_NAME = "remoteName";
    public static final String KEY_UNFOLLOW = "unfollow";
    public static final String FILE_NAME_DATA = "data.json";
    
    public static final String NAME_APP = "KAWASEMI GIT v1.5.5";

    JFileChooser chooser;
    String choosertitle;
    
    
    private JLabel lblNewLabel;
    
    private JList<String> list;
    private JTextArea textArea;
    
    private JComboBox<String> comboBoxRemote;
    
    private PrintStream standardOut;

    private JButton btnClean;// clean button
    private JButton btnSelectWorkspace;// select workspace
    
    
    private JButton btnCheckout;
    private JButton btnDiscard;
    private JButton btnCreateBranch;
    private JButton btnPull;
    private JButton btnMerge;
    private JButton btnRefresh;
    private JButton btnFetch;
    private JButton btnUnfollow;
    private JButton btnFollowAll;
    private JButton btnStop;
    
    private JFrame frame;
    private JSONObject dataJson = null;
    
    private JProgressBar progressBar;
    private int totalProgress = 0;
    private int currentProgress = 0;
    private int coutProressFillList = 0;
    
    // list selected
    private List<String> selectedListCustom;
    private int[] selectedIndexs;
    
    private List<String> listRepoError = new LinkedList<String>();
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {

                try {
                    main window = new main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public main() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {

        // create file data
        File tempFile = new File(FILE_NAME_DATA);
        boolean exists = tempFile.exists();
        // data.json not exist
        if (exists == false) {
            JSONObject dataOb = new JSONObject();
            dataOb.put(KEY_PATH_WORKSPACE, "");
            dataOb.put(KEY_REMOTE_NAME, "");
            JSONArray unfollow = new JSONArray();
            dataOb.put(KEY_UNFOLLOW, unfollow);
            // Write JSON file
            try (FileWriter file = new FileWriter(FILE_NAME_DATA)) {
                file.write(dataOb.toJSONString());
                file.flush();
            } catch (IOException ea) {
                ea.printStackTrace();
            }
            this.readFileData();
        } else { // data.json exist
            this.readFileData();
            if(main.this.getValueByKeyData(KEY_REMOTE_NAME) == null) {
                main.this.setValueByKeyData(KEY_REMOTE_NAME, "");
            }
            if(main.this.getValueByKeyData(KEY_PATH_WORKSPACE) == null) {
                main.this.setValueByKeyData(KEY_PATH_WORKSPACE, "");
            }
            if(main.this.getValueByKeyData(KEY_UNFOLLOW) == null) {
                JSONArray unfollow = new JSONArray();
                main.this.setValueByKeyData(KEY_UNFOLLOW, unfollow);
            }
        }

        frame = new JFrame();
        frame.setResizable(false);
        frame.setTitle(NAME_APP);
        frame.setBounds(100, 100, 970, 630);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        btnCheckout = new JButton("Checkout");
        btnCheckout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent a) {
                String branchName = JOptionPane.showInputDialog(frame, "Nhập vào tên branch");

                if(branchName != null && !branchName.trim().equals("")) {
                    List<String> listSelectedRepo = main.this.getSelectedValuesList();
                    main.this.totalProgress = listSelectedRepo.size();
                    main.this.currentProgress = 0;
                    // clear list repo error
                    main.this.listRepoError.clear();
                    for (String repoName : listSelectedRepo) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String repoError = main.this.checkOutAndPullByNameBranch(repoName, branchName);
                                if(!repoError.equals("")) main.this.listRepoError.add(repoError);
                                main.this.currentProgress = main.this.currentProgress + 1;
                            }
                        }).start();
                    }
                    main.this.fillProgressBar();
                }
            }
        });
        btnCheckout.setBounds(651, 176, 139, 78);
        frame.getContentPane().add(btnCheckout);

        btnSelectWorkspace = new JButton("Select workspace");
        btnSelectWorkspace.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent a) {

                //uncheck
                main.this.selectedIndexs = null;
                chooser = new JFileChooser();
                
                if(main.this.dataJson != null) {
                    chooser.setCurrentDirectory(new java.io.File(main.this.getValueByKeyData(KEY_PATH_WORKSPACE)));
                }else {
                    chooser.setCurrentDirectory(new java.io.File("."));
                }
                chooser.setDialogTitle(choosertitle);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                //
                // disable the "All files" option.
                //
                chooser.setAcceptAllFileFilterUsed(false);
                //
                if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                    main.this.dataJson.replace(KEY_PATH_WORKSPACE, chooser.getSelectedFile().toString());
                    main.this.writeFileData(main.this.dataJson);
                    main.this.lblNewLabel.setText(chooser.getSelectedFile().toString());

                    // fill list
                    if(new File(main.this.getValueByKeyData(KEY_PATH_WORKSPACE)).exists()) {
                        main.this.fillDataToList(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));
                    }
                    
                } else {
                    //System.out.println("No Selection ");
                }
            }
        });
        btnSelectWorkspace.setBounds(230, 11, 211, 23);
        frame.getContentPane().add(btnSelectWorkspace);

        this.lblNewLabel = new JLabel("");
        
        if(this.dataJson != null) {
            this.lblNewLabel.setText(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));
        }
        
        lblNewLabel.setBounds(535, 15, 394, 14);
        frame.getContentPane().add(lblNewLabel);

        btnDiscard = new JButton("Discard");
        btnDiscard.setBackground(Color.RED);
        btnDiscard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List<String> listSelectedRepo = main.this.getSelectedValuesList();
                main.this.totalProgress = listSelectedRepo.size();
                main.this.currentProgress = 0;
                for (String repoName : listSelectedRepo) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            main.this.discardRepoByName(repoName);
                            main.this.currentProgress = main.this.currentProgress + 1;
                        }
                    }).start();
                }
                main.this.fillProgressBar();
            }
        });
        btnDiscard.setBounds(800, 77, 139, 78);
        frame.getContentPane().add(btnDiscard);


        
        btnCreateBranch = new JButton("Create branch");
        btnCreateBranch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                JTextField fromNameBranch = new JTextField();
                JTextField newNameBranch = new JTextField();
                final JComponent[] inputs = new JComponent[] {
                        new JLabel("Tạo branch từ: "),
                        fromNameBranch,
                        new JLabel("Tên branch mới:"),
                        newNameBranch
                };
                int result = JOptionPane.showConfirmDialog(frame, inputs, "Tạo branch", JOptionPane.CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    if(!fromNameBranch.getText().trim().equals("") && !newNameBranch.getText().trim().equals("")) {
                        List<String> listSelectedRepo = main.this.getSelectedValuesList();
                        main.this.totalProgress = listSelectedRepo.size();
                        main.this.currentProgress = 0;
                        main.this.listRepoError.clear();
                        for (String repoName : listSelectedRepo) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String repoError = main.this.checkOutAndPullByNameBranch(repoName, fromNameBranch.getText().trim());
                                    if(!repoError.equals("")) {
                                        main.this.listRepoError.add(repoError);
                                    }else {
                                        main.this.createNewBranch(repoName, newNameBranch.getText().trim());
                                    }
                                    main.this.currentProgress = main.this.currentProgress + 1;
                                }
                            }).start();
                        }
                        main.this.fillProgressBar();
                    }
                }
            }
        });
        btnCreateBranch.setBounds(502, 176, 139, 78);
        frame.getContentPane().add(btnCreateBranch);
        
        btnPull = new JButton("PULL");
        btnPull.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                List<String> listSelectedRepo = main.this.getSelectedValuesList();
                main.this.totalProgress = listSelectedRepo.size();
                main.this.currentProgress = 0;
                for (String repoName : listSelectedRepo) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            main.this.gitPull(repoName);
                            main.this.currentProgress = main.this.currentProgress + 1;
                        }
                    }).start();
                }
                main.this.fillProgressBar();
            }
        });
        btnPull.setBounds(502, 77, 139, 78);
        frame.getContentPane().add(btnPull);
        
        this.textArea = new JTextArea();
        //textArea.setBounds(230, 221, 699, 320);
        textArea.setEditable(false);
        PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));
        // keeps reference of standard output stream
        standardOut = System.out;
         
        // re-assigns standard output stream and error output stream
        System.setOut(printStream);
        System.setErr(printStream);
        
        JScrollPane jScrollPane = new JScrollPane(textArea);
        jScrollPane.setBounds(359, 300, 570, 250);
        
        frame.getContentPane().add(jScrollPane);
        
        this.btnClean = new JButton("Clear log");
        btnClean.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             // clears the text area
                try {
                    textArea.getDocument().remove(0,
                            textArea.getDocument().getLength());
                    standardOut.println("Text area cleared");
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnClean.setBounds(260, 525, 89, 23);
        frame.getContentPane().add(btnClean);
        
        JLabel lblNewLabel_1 = new JLabel("Workspace: ");
        lblNewLabel_1.setBounds(450, 15, 98, 14);
        frame.getContentPane().add(lblNewLabel_1);
        
        JLabel lblNewLabel_2 = new JLabel("Remote: ");
        lblNewLabel_2.setBounds(450, 49, 68, 14);
        frame.getContentPane().add(lblNewLabel_2);
        
        
        comboBoxRemote = new JComboBox();
        comboBoxRemote.setBounds(535, 46, 89, 20);
        frame.getContentPane().add(comboBoxRemote);

        // select combobox by value
        main.this.setSelectedValueCombobox(comboBoxRemote, main.this.getValueByKeyData(KEY_REMOTE_NAME));
        
        btnMerge = new JButton("Merge");
        btnMerge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List<String> listSelectedRepo = main.this.getSelectedValuesList();
                
                String listRemote[] = main.this.getListRemote(listSelectedRepo.get(0));
                JComboBox<String> fromRemoteCombobox = new JComboBox<String>(listRemote);
                main.this.setSelectedValueCombobox(fromRemoteCombobox, main.this.getValueByKeyData(KEY_REMOTE_NAME));
                
                JTextField fromNameBranch = new JTextField();
                
                final JComponent[] inputs = new JComponent[] {
                        new JLabel("Remote: "),
                        fromRemoteCombobox,
                        new JLabel("Merge từ branch: "),
                        fromNameBranch
                };
                int result = JOptionPane.showConfirmDialog(frame, inputs, "Merge branch", JOptionPane.CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    if(!fromNameBranch.getText().trim().equals("")) {
                        main.this.totalProgress = listSelectedRepo.size();
                        main.this.currentProgress = 0;
                        for (String repoName : listSelectedRepo) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    main.this.merge(repoName, fromNameBranch.getText().trim(), fromRemoteCombobox.getSelectedItem().toString());
                                    main.this.currentProgress = main.this.currentProgress + 1;
                                }
                            }).start();
                        }
                        main.this.fillProgressBar();
                    }
                }
            }
        });
        btnMerge.setBounds(800, 176, 139, 78);
        frame.getContentPane().add(btnMerge);
        
        progressBar = new JProgressBar();
        progressBar.setBounds(359, 275, 570, 14);

        frame.getContentPane().add(progressBar);
        comboBoxRemote.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                // get value combobox
                main.this.setValueByKeyData(KEY_REMOTE_NAME, main.this.comboBoxRemote.getSelectedItem().toString());
            }
        });

        main.this.progressBar.setStringPainted(true);

        btnRefresh = new JButton("Refresh");
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                main.this.selectedIndexs = main.this.list.getSelectedIndices();
                // fill list
                if(new File(main.this.getValueByKeyData(KEY_PATH_WORKSPACE)).exists()) {
                    main.this.fillDataToList(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));
                }
            }
        });
        btnRefresh.setBounds(10, 11, 210, 23);
        frame.getContentPane().add(btnRefresh);

        btnFetch = new JButton("Fetch");
        btnFetch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                List<String> listSelectedRepo = main.this.getSelectedValuesList();
                main.this.totalProgress = listSelectedRepo.size();
                main.this.currentProgress = 0;
                for (String repoName : listSelectedRepo) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            main.this.fetchAll(repoName);
                            main.this.currentProgress = main.this.currentProgress + 1;
                        }
                    }).start();
                }
                main.this.fillProgressBar();
            }
        });
        btnFetch.setBounds(651, 77, 139, 78);
        frame.getContentPane().add(btnFetch);
        
        
        btnUnfollow = new JButton("Unfollow");
        btnUnfollow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                main.this.selectedIndexs = main.this.list.getSelectedIndices();
                JSONArray listUnfollow = (JSONArray)main.this.dataJson.get(KEY_UNFOLLOW);
                for (int i = main.this.selectedIndexs.length-1; i >= 0; i--) {
                 // add to file data.json
                    listUnfollow.add(main.this.list.getModel().getElementAt(selectedIndexs[i]).split("<")[0]);
                    ((DefaultListModel) main.this.list.getModel()).remove(selectedIndexs[i]);
                }
                if (listUnfollow != null) {
                    main.this.dataJson.replace(KEY_UNFOLLOW, listUnfollow);
                }else {
                    main.this.dataJson.replace(KEY_UNFOLLOW, new JSONArray());
                }
                main.this.writeFileData(main.this.dataJson);
                main.this.readFileData();
                main.this.disableAllButton();
                main.this.list.setEnabled(true);
                main.this.btnFollowAll.setEnabled(true);
                main.this.btnRefresh.setEnabled(true);
                main.this.btnSelectWorkspace.setEnabled(true);
                main.this.comboBoxRemote.setEnabled(true);
            }
        });
        btnUnfollow.setBounds(352, 45, 89, 23);
        frame.getContentPane().add(btnUnfollow);
        
        btnFollowAll = new JButton("Follow all");
        btnFollowAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                main.this.selectedIndexs = null;
                main.this.dataJson.replace(KEY_UNFOLLOW, new JSONArray());
                main.this.writeFileData(dataJson);
                main.this.readFileData();
                // fill list
                if(new File(main.this.getValueByKeyData(KEY_PATH_WORKSPACE)).exists()) {
                    main.this.fillDataToList(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));
                }
            }
        });
        btnFollowAll.setBounds(352, 77, 89, 23);
        frame.getContentPane().add(btnFollowAll);
        
        
        // list
        list = new JList();
        list.setFont(new Font("Arial",Font.PLAIN,13));
        
        if(new File(main.this.getValueByKeyData(KEY_PATH_WORKSPACE)).exists()) {
            main.this.fillDataToList(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));
        }else {
            main.this.disableAllButton();
            main.this.btnSelectWorkspace.setEnabled(true);
            main.this.comboBoxRemote.setEnabled(true);
        }
        
        list.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting()) {
                    main.this.enabledAllButton();
                }
            }
        });
        
        JScrollPane scrollPane = new JScrollPane(list);
        scrollPane.setBounds(9, 45, 340, 400);
        frame.getContentPane().add(scrollPane);
        
        btnStop = new JButton("Stop");
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                main.this.currentProgress = main.this.totalProgress;
                main.this.coutProressFillList = 0;
            }
        });
        btnStop.setBounds(352, 105, 89, 23);
        frame.getContentPane().add(btnStop);

    }

    private void fillProgressBar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    main.this.progressBar.setValue(0);
                    main.this.disableAllButton();
                    while (main.this.totalProgress != main.this.currentProgress) {
                        // fill the menu bar 
                        int percent = ((main.this.currentProgress*100)/main.this.totalProgress);
                        
                        if(percent == 0) {
                            main.this.progressBar.setString("Loading.............");
                        }else {
                            main.this.progressBar.setString(String.valueOf(percent) + "%");
                        }

                        main.this.progressBar.setValue(percent);
                        // delay the thread 
                        Thread.sleep(50);
                    }
                    main.this.progressBar.setValue(100);
                    main.this.progressBar.setString("DONE");
                    main.this.fillDataToList(main.this.getValueByKeyData(KEY_PATH_WORKSPACE));

                    //checkout
                    if(main.this.listRepoError.size() > 0) {
                        String messenger = "";
                        for (String repoError : listRepoError) {
                            messenger = messenger + repoError + "\n";
                        }
                        JOptionPane.showConfirmDialog(frame, messenger, "List repository cannot checkout", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                        //debug
                        main.this.listRepoError.clear();
                    }
                } 
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    
    private void setSelectedValueCombobox(JComboBox comboBox, String value)
    {
        String item;
        for (int i = 0; i < comboBox.getItemCount(); i++)
        {
            item = (String)comboBox.getItemAt(i);
            if (item.equals(value))
            {
                comboBox.setSelectedIndex(i);
                break;
            }
        }
    }
    
    private void disableAllButton() {
        btnCheckout.setEnabled(false);
        btnSelectWorkspace.setEnabled(false);
        btnDiscard.setEnabled(false);
        btnCreateBranch.setEnabled(false);
        btnPull.setEnabled(false);
        btnMerge.setEnabled(false);
        btnFetch.setEnabled(false);
        comboBoxRemote.setEnabled(false);
        list.setEnabled(false);
        btnRefresh.setEnabled(false);
        btnUnfollow.setEnabled(false);
        btnFollowAll.setEnabled(false);
    }
    private void enabledAllButton() {
        btnCheckout.setEnabled(true);
        btnSelectWorkspace.setEnabled(true);
        btnDiscard.setEnabled(true);
        btnCreateBranch.setEnabled(true);
        btnPull.setEnabled(true);
        btnMerge.setEnabled(true);
        btnFetch.setEnabled(true);
        comboBoxRemote.setEnabled(true);
        list.setEnabled(true);
        btnRefresh.setEnabled(true);
        btnUnfollow.setEnabled(true);
        btnFollowAll.setEnabled(true);
    }
    
    private void gitPull(String repoName) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);
            //git rev-parse --abbrev-ref HEAD
            String nameBranch = runCommand(directory, "git rev-parse --abbrev-ref HEAD");
            //git fetch --progress --prune --recurse-submodules=no origin;
            runCommand(directory, "git fetch --progress --prune --recurse-submodules=no " + main.this.getValueByKeyData(KEY_REMOTE_NAME));
            //git merge --ff origin/version/101012;
            runCommand(directory, "git merge --ff "+ main.this.getValueByKeyData(KEY_REMOTE_NAME) +"/" + nameBranch);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }
    
    private void discardRepoByName(String repoName) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);
            runCommand(directory, "git reset --hard");
            runCommand(directory, "git clean -f -d");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private String checkOutAndPullByNameBranch(String repoName, String nameBranch) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);
            
            Boolean isError = false;

            // git checkout branch name
            String outString = runCommand(directory, "git checkout " + nameBranch);
            if(outString.contains("did not match any file(s) known to git")) {
                // git fetch
                runCommand(directory, "git fetch " + main.this.getValueByKeyData(KEY_REMOTE_NAME));
                // git checkout -t origin/namebranch
                String outStringCheckoutRemote = runCommand(directory, "git checkout -t "+ main.this.getValueByKeyData(KEY_REMOTE_NAME) +"/" + nameBranch);
                
                if(outStringCheckoutRemote.contains("fatal") || outStringCheckoutRemote.contains("error")){
                    isError = true;
                }
            }

            if(outString.contains("error") || outString.contains("fatal")) {
                isError = true;
            }

            if(isError.equals(false)) {
                // pull
                //git fetch --progress --prune --recurse-submodules=no origin;
                //git merge --ff origin/766/water_101011;
                runCommand(directory, "git fetch --progress --prune --recurse-submodules=no " + main.this.getValueByKeyData(KEY_REMOTE_NAME));
                runCommand(directory, "git merge --ff "+ main.this.getValueByKeyData(KEY_REMOTE_NAME) +"/" + nameBranch);
            }

            if(isError) {
                return repoName;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }
    
    private void merge(String repoName, String nameBranch, String remote) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);

            //git fetch --progress --prune --recurse-submodules=no origin
            runCommand(directory, "git fetch --progress --prune --recurse-submodules=no " + remote);

            //git merge --no-commit --no-ff origin/master
            runCommand(directory, "git merge --no-commit --no-ff "+ remote +"/" + nameBranch);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private void createNewBranch(String repoName, String nameBranch) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);

            //git branch --no-track version/101017;
            runCommand(directory, "git branch --no-track " + nameBranch);
            //git checkout version/101017;
            runCommand(directory, "git checkout " + nameBranch);
            //git push --porcelain --progress origin refs/heads/version/101017:refs/heads/version/101017
            runCommand(directory, "git push --porcelain --progress "+ main.this.getValueByKeyData(KEY_REMOTE_NAME) +" refs/heads/"+ nameBranch +":refs/heads/"+ nameBranch +"");
            
            //git branch --set-upstream-to=origin/branch3 branch3
            runCommand(directory, "git branch --set-upstream-to="+ main.this.getValueByKeyData(KEY_REMOTE_NAME) +"/"+ nameBranch +" " + nameBranch);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private void fetchAll(String repoName) {

        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);

            //git fetch --progress --prune --recurse-submodules=no origin
            runCommand(directory, "git fetch --progress --prune --recurse-submodules=no " + main.this.getValueByKeyData(KEY_REMOTE_NAME));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private String getNameBranchByRepo(String repoName) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);

            //git rev-parse --abbrev-ref HEAD
            String nameBranch = runCommand(directory, "git rev-parse --abbrev-ref HEAD");
            return nameBranch;
            
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private String[] getListRemote(String repoName) {
        try {
            Path directory = Paths
                    .get(this.getValueByKeyData(KEY_PATH_WORKSPACE) + "\\" + repoName);

            //git remote
            String stringOut = runCommand(directory, "git remote");
            
            String lines[] = stringOut.split("\\|");

            return lines;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private void fillDataToList(String path) {
        DefaultListModel listModel = new DefaultListModel();
        // get list folder
        File file = new File(path);
        String[] directories = file.list(new FilenameFilter() {
          @Override
          public boolean accept(File current, String name) {
            return new File(current, name).isDirectory();
          }
        });
        
        // loop
        int i = 0;
        main.this.coutProressFillList = 0;
        List<Thread> listThread = new LinkedList<Thread>();
        for (String directorie : directories) {
            //
            Boolean isExistFollow = false;
            String pathGit = path + "\\" + directorie + "\\" + ".git";
            File tempFile = new File(pathGit);
            boolean exists = tempFile.exists();
            if(exists == true) {
                // check list unfollow
                JSONArray listUnfollow = (JSONArray)main.this.dataJson.get(KEY_UNFOLLOW);
                Iterator<String> iterator = listUnfollow.iterator();
                while (iterator.hasNext()) {
                    if(iterator.next().equals(directorie)) {
                        isExistFollow = true;
                    }
                }
                // add elemment
                if(isExistFollow == false) {
                    // get git remote
                    if(i == 0) {
                        String nameRemotes[] = main.this.getListRemote(directorie);
                        if(nameRemotes != null) {
                            if(main.this.getValueByKeyData(KEY_REMOTE_NAME).equals("")) {
                                main.this.setValueByKeyData(KEY_REMOTE_NAME, nameRemotes[0]);
                            }
                            // set label
                            DefaultComboBoxModel dm = new DefaultComboBoxModel(nameRemotes);
                            comboBoxRemote.setModel(dm);
                            
                            if(!main.this.getValueByKeyData(KEY_REMOTE_NAME).equals("")) {
                                main.this.setSelectedValueCombobox(comboBoxRemote, main.this.getValueByKeyData(KEY_REMOTE_NAME));
                            }
                        }
                        i++;
                    }

                    listThread.add(new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                            listModel.addElement(directorie + "<" + main.this.getNameBranchByRepo(directorie) + ">");
                            main.this.coutProressFillList--;
                        }
                    }));
                    
                }
            }
        }
        
        main.this.coutProressFillList = listThread.size();
        
        for (Thread thread : listThread) {
            thread.start();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                
                // waiting thread add itemt to list done
                while(main.this.coutProressFillList != 0 ) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                main.this.sortList(listModel);
                main.this.list.setModel(listModel);
                main.this.disableAllButton();
                main.this.list.setEnabled(true);
                main.this.btnFollowAll.setEnabled(true);
                main.this.btnRefresh.setEnabled(true);
                main.this.btnSelectWorkspace.setEnabled(true);
                main.this.comboBoxRemote.setEnabled(true);
                main.this.selectList();
            }
        }).start();
    }
    
    public DefaultListModel sortList(DefaultListModel model) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < model.size(); i++) {
            list.add((String)model.get(i));
        }
        Collections.sort(list);
        model.removeAllElements();
        for (String s : list) {
            model.addElement(s);
        }
        return model;
     }

    private void readFileData() {
        // JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(FILE_NAME_DATA)) {
            // Read JSON file
            Object obj = jsonParser.parse(reader);

            this.dataJson = (JSONObject) obj;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void writeFileData(JSONObject dataJson) {
        // Write JSON file
        try (FileWriter file = new FileWriter(FILE_NAME_DATA)) {

            file.write(dataJson.toJSONString());
            file.flush();

        } catch (IOException ea) {
            ea.printStackTrace();
        }
    }
    
    private String getValueByKeyData(String key){
        if(main.this.dataJson.get(key) == null) {
            return null;
        }
        return main.this.dataJson.get(key).toString();
    }
    
    private void setValueByKeyData(String key, Object value) {
        if(main.this.dataJson.get(key) == null) {
            main.this.dataJson.put(key, value);
        }else {
            main.this.dataJson.replace(key, value);
        }
        
        main.this.writeFileData(main.this.dataJson);
        main.this.readFileData();
    }
    
    
    private List<String> getSelectedValuesList() {
        main.this.selectedListCustom = new LinkedList<String>();
        // set selectedIndexs
        main.this.selectedIndexs = main.this.list.getSelectedIndices();
        List<String> listSelectedRepo = main.this.list.getSelectedValuesList();
        for (String repoName : listSelectedRepo) {
            String repoNameCutom = repoName.split("<")[0];
            selectedListCustom.add(repoNameCutom);
        }

        return selectedListCustom;
        
    }
    
    private void selectList() {
        if (main.this.selectedIndexs != null) {
            list.setSelectedIndices(main.this.selectedIndexs);
        }
    }

    public String runCommand(Path directory, String commandString) throws IOException, InterruptedException {
        System.out.println(directory+": " + commandString);
        Objects.requireNonNull(directory, "directory");
        if (!Files.exists(directory)) {
            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
        }
        String[] command = commandString.split(" ");
        ProcessBuilder pb = new ProcessBuilder().command(command).directory(directory.toFile());
        pb.redirectErrorStream(true);
        Process p = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line;
        String stringOut = "";
        int i = 0;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            if(i == 0) {
                stringOut = stringOut + line;
            }else {
                stringOut = stringOut + "|" + line;
            }
            i++;
        }
        return stringOut;
    }
}
